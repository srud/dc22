---
name: Accommodation
---

# Accommodation

Attendees with an [accommodation bursary](/about/bursaries/) will be
accommodated at the [Innovation and Training Park](https://itp-prizren.com/).

The conference [venue](https://debconf22.debconf.org/about/venue/) and accommodation facilities are both inside the Innovation
and Training Park.

<i class="fa fa-map" aria-hidden="true"></i>
[Map](https://www.openstreetmap.org/#map=16/42.2211/20.7487)

The bedrooms will be shared and will accommodate 2 people.
The bathrooms are also shared.
See these [pictures](https://wiki.debian.org/DebConf/22/Accommodation/pictures) of the facilities: 

Wi-Fi will be available at the accommodation facilities.
## Self-paid accommodation

DebConf is providing beds for people who want to pay for accommodation at ITP.
The cost is 15 Eur/night. 
If you want a separate room just for yourself, the cost is 30 Eur/night.
If number of rooms is filled, we will share that information here.

##  Hotels

If you are looking for a hotel in Prizren here is a list:

 * [City Hotel](http://www.cityhotelks.com/index.php)
 * [Hotel Premium Park](http://www.premiumparkhotel.com/)
 * [Hotel Tiffany](http://hoteltiffanyprizren.com/?lang=en)
 * [Hotel Centrum](http://hotelcentrumprizren.com/)
 * [Hotel Kacinari](https://hotelkacinari.com/)
 * [Hotel Albatros](http://www.hotel-albatros.net/)
 * [Hotel Abi](https://www.trip.com/hotels/prizren-hotel-detail-9487126/abi-hotel/)
 * [Deni House](https://www.booking.com/hotel/xk/deni-house.en-gb.html?aid=356980;label=gog235jc-1DCAsogQJCCmRlbmktaG91c2VIM1gDaIECiAEBmAEJuAEZyAEP2AED6AEBiAIBqAIDuAKT17OMBsACAdICJDNmMmM2M2I5LTQ4OTEtNDIyZS05ZjUzLWQxMzNlMDU2ZjkyNNgCBOACAQ;sid=18a5cedaa59b8fd44dd09b2ae241d5e5;dist=0;group_adults=2;group_children=0;keep_landing=1;no_rooms=1;sb_price_type=total;type=total;sig=v1LInhVKBW&)
 * [Hotel Prizreni](https://hotelprizreni.com/)
 * [Hotel Edi Imperial](http://ediimperial.com/)
 * [Hotel Antika](https://www.booking.com/hotel/xk/antika.en-gb.html?label=gen173rf-1BCAsogQJCBmFudGlrYUgzWANoBogBAZgBCbgBGcgBD9gBAegBAYgCAaICEXZpc2l0LXByaXpyZW4uY29tqAIDuAKnloKTBsACAdICJDdhODkyOGMzLWI5YjgtNDBiYS1hNzE2LTk3ZTg2MzY1YzRkNNgCBeACAQ&sid=0d2e193afb5ff8dc09269b8112c96355&dist=0&keep_landing=1&sb_price_type=total&type=total&)
 * [Hotel Cleon](https://www.booking.com/hotel/xk/cleon.en-gb.html?aid=356980;label=gog235jc-1DCAsogQJCBWNsZW9uSDNYA2iBAogBAZgBCbgBGcgBD9gBA-gBAYgCAagCA7gC7dizjAbAAgHSAiQ1NTQ0NDBiNi1kZjljLTQ5ZjYtOTU5NS03MjllNWFlNzUxMzTYAgTgAgE;sid=18a5cedaa59b8fd44dd09b2ae241d5e5;dist=0;group_adults=2;group_children=0;keep_landing=1;no_rooms=1;sb_price_type=total;type=total;sig=v1QSTsx1wc&)
 * [Hotel Fjorr](https://www.booking.com/hotel/xk/fjorr.en-gb.html?aid=356980;label=gog235jc-1DCAsogQJCBWZqb3JySDNYA2iBAogBAZgBCbgBGcgBD9gBA-gBAYgCAagCA7gCg9mzjAbAAgHSAiQ2Y2U1OWIyNC1iMDNmLTRhNGQtOGRlMi05YzdlN2U3YmRkNDPYAgTgAgE;sid=18a5cedaa59b8fd44dd09b2ae241d5e5;dist=0;group_adults=2;group_children=0;keep_landing=1;no_rooms=1;sb_price_type=total;type=total;sig=v1Zh1Le3m6&)
 * [Ura Hostel](https://www.booking.com/hotel/xk/ura-hostel.en-gb.html?aid=356980;label=gog235jc-1DCAsogQJCCnVyYS1ob3N0ZWxIM1gDaIECiAEBmAEJuAEZyAEP2AED6AEBiAIBqAIDuAKj2LOMBsACAdICJDM3ZTMzNmQ4LTBkODYtNDZkNi1iM2VhLWJiYjRiYjNkMWU0Y9gCBOACAQ;sid=18a5cedaa59b8fd44dd09b2ae241d5e5;dist=0;group_adults=2;group_children=0;keep_landing=1;no_rooms=1;sb_price_type=total;type=total;sig=v1_xnkHv6J&)
 * [Hostel Driza's house](https://drizas-house.com/)
 * [Guesthouse Hotel My Home](https://www.booking.com/hotel/xk/guesthouse-my-home.en-gb.html)
 
You may book a room in nearby hotels or hostels in Prizren.
If you don’t see any availability on their website, we’d suggest
checking booking aggregator websites or contacting the hotel directly.
